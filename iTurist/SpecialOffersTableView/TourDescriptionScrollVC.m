//
//  DescriptionScrolViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//
#define IMAGE_WIDTH 240
#define IMAGE_HEIGHT 130

#import "TourDescriptionScrollVC.h"
#import "SBJson.h"
#import "SBJsonWriter.h"
#import "MyCell.h"
#import "TourDescriptionCell.h"
#import "OrderTourOrAuctionVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"

@interface TourDescriptionScrollVC (){
    SDWebImageManager *manager;
}

@end

@implementation TourDescriptionScrollVC

@synthesize slidesPhotosArray;
@synthesize slidesUrlArray;
@synthesize scrollView;
@synthesize firstTourInfo;
@synthesize secondTourInfo;
@synthesize tourInfoArray;
@synthesize starsCount;
@synthesize tourId;
@synthesize nameTourDescriptionLabelString;
@synthesize peopleNightDescriptionLabelString;
@synthesize moneyDescriptionLabelString;
@synthesize nameTourDescriptionLabel;
@synthesize peopleNightDescriptionLabel;
@synthesize moneyDescriptionLabel;
@synthesize prezentationDescriptionImageView;
@synthesize imageUrl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    slidesPhotosArray = [NSMutableArray new];
    manager = [SDWebImageManager new];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    scrollView.contentSize = CGSizeMake(320.0f, 950.0f);
    [self requests];
    NSData *data = [NSData dataWithContentsOfURL:imageUrl];
    prezentationDescriptionImageView.image = [UIImage imageWithData:data];
    firstTourInfo = [[UITableView alloc] initWithFrame:CGRectMake(18.0f, 250.0f, 283.0f, 152.5f) style:UITableViewStylePlain];
    firstTourInfo.backgroundColor=[UIColor clearColor];
    firstTourInfo.rowHeight = 38;
    firstTourInfo.scrollEnabled = NO;
    firstTourInfo.separatorStyle=UITableViewCellSeparatorStyleNone;
    firstTourInfo.delegate = self;
    firstTourInfo.dataSource = self;
    [firstTourInfo.layer setCornerRadius:10.0f];
    [scrollView addSubview:firstTourInfo];
    scrollView.backgroundColor=[UIColor clearColor];
    secondTourInfo = [[UITableView alloc] initWithFrame:CGRectMake(18.0f, 420.0f, 283.0f, 342.0f) style:UITableViewStylePlain];
    secondTourInfo.backgroundColor=[UIColor clearColor];
    secondTourInfo.rowHeight = 38;
    secondTourInfo.scrollEnabled = NO;
    secondTourInfo.separatorStyle=UITableViewCellSeparatorStyleNone;
    secondTourInfo.delegate = self;
    secondTourInfo.dataSource = self;
    [secondTourInfo.layer setCornerRadius:10.0f];
    [scrollView addSubview:secondTourInfo];
    UIButton *but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    but.frame= CGRectMake(30, 780, 257, 40);
    [but setTitle:@"" forState:UIControlStateNormal];
    [but addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [but setBackgroundImage:[UIImage imageNamed:@"zakaz.png"] forState:UIControlStateNormal];
    [scrollView addSubview:but];
    }

-(void)buttonAction
{
    [self performSegueWithIdentifier:@"cash" sender:nil];
}

-(void)showSlider
{
    int i=0;
    while ([slidesPhotosArray count]>i)
    {
        UIImage *images = [slidesPhotosArray objectAtIndex:i];
        UIImageView *imageView;
        imageView = [[UIImageView alloc] initWithImage:images];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.clipsToBounds = YES;
        imageView.tag = 1;
        UIScrollView *scrollView1 = [[UIScrollView alloc] initWithFrame:CGRectMake( IMAGE_WIDTH * i++, 0, IMAGE_WIDTH, IMAGE_HEIGHT)];
        scrollView1.delegate = self;
        imageView.frame = scrollView1.bounds;
        [scrollView1 addSubview:imageView];
        [self.photoScrollView addSubview:scrollView1];
    }
    self.photoScrollView.contentSize = CGSizeMake(IMAGE_WIDTH*i, IMAGE_HEIGHT);
    self.photoScrollView.delegate = self;
    prezentationDescriptionImageView.image = nil;
    [MBProgressHUD hideAllHUDsForView:_photoScrollView animated:YES];
}

-(void)loadSliderPhotos
{
    [MBProgressHUD showHUDAddedTo:_photoScrollView animated:YES];
    [slidesPhotosArray addObject:prezentationDescriptionImageView.image];
    NSLog(@"%@", slidesPhotosArray);
    int i=0;
    while ([slidesUrlArray count]>i) {
        NSURL *url = [NSURL URLWithString:[slidesUrlArray objectAtIndex:i]];
        [manager downloadWithURL:url
                         options:0
                        progress:^(NSUInteger receivedSize, long long expectedSize){}
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
         {
             if (image)
             {
                 [slidesPhotosArray addObject:image];
                 if (slidesPhotosArray.count==slidesUrlArray.count+1) {
                     [self showSlider];
                 }
             }
         }];
        i++;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"cash"])
    {
        OrderTourOrAuctionVC *detailViewController = [segue destinationViewController];
        detailViewController.tourId=tourId;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    nameTourDescriptionLabel.text = [NSString stringWithFormat:@"%@", nameTourDescriptionLabelString];
    peopleNightDescriptionLabel.text =  peopleNightDescriptionLabelString;
    moneyDescriptionLabel.text = moneyDescriptionLabelString;
}

- (void)requests
{
    NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:tourId forKey:@"tour_id"];
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getTourInfo&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection)
    {
        NSLog(@"Connecting...");
        receivedData =  [NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                   [error localizedDescription],
                   [error description],
                   [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    tourInfoDic = [NSMutableDictionary new];
    tourInfoDic=[parser objectWithString:dataString][@"response"];
    tourInfoArray = [NSMutableArray new];
    [tourInfoArray addObject:tourInfoDic[@"hotel_desc"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_location"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_in_room"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_services"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_restaurants"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_sports"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_beach"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_for_pay"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_comments"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_number"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_food"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_transfer"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_flights"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_telephone"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_touroperator"]];
    [tourInfoArray addObject:tourInfoDic[@"hotel_site"]];
    slidesUrlArray =tourInfoDic[@"slides"];
    NSLog(@"%@", slidesUrlArray);
    if (![tourInfoDic[@"slides"] isKindOfClass:[NSDecimalNumber class]]) {
        [slidesUrlArray addObject:tourInfoDic[@"image_url"]];
        NSLog(@"%@", slidesUrlArray);
        [self loadSliderPhotos];
    }
    [self.scrollView reloadInputViews];
    [firstTourInfo reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == firstTourInfo) {
        return 4;
    }else {
        return 9;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TourDescriptionCell";
    TourDescriptionCell *cellqq = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cellqq == nil)
    {
        cellqq = [[TourDescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (tableView == firstTourInfo) {
        cellqq.selectionStyle=UITableViewCellSelectionStyleNone;
        cellqq.infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 280, 20)];
        cellqq.infoLabel.backgroundColor = [UIColor clearColor];
        cellqq.infoLabel.font = [UIFont boldSystemFontOfSize:13];
        UILabel *InfataLabel = [UILabel new];
        InfataLabel.textColor = [UIColor  colorWithRed:80.0f/255.0f green:98.0f/255.0f blue:194.0f/255.0f alpha:1.0f];
        InfataLabel.backgroundColor = [UIColor clearColor];
        switch (indexPath.row)
    
    {
        case 0:{
            cellqq.infoLabel.text =@"Страна:";
            InfataLabel.frame=CGRectMake(63, 10, 280, 20);
            if ([tourInfoDic[@"country_name"] isKindOfClass:[NSString class]])
            InfataLabel.text = tourInfoDic[@"country_name"];
            [cellqq addSubview:InfataLabel];
            break;}
        case 1:{
            cellqq.infoLabel.text =@"Город/Курорт:";
            InfataLabel.frame=CGRectMake(107, 10, 280, 20);
            if ([tourInfoDic[@"city_name"] isKindOfClass:[NSString class]])
            InfataLabel.text = tourInfoDic[@"city_name"];
            [cellqq addSubview:InfataLabel];
            break;}
        case 2:{
            cellqq.infoLabel.text =@"Отель:";
            InfataLabel.frame=CGRectMake(56, 10, 280, 20);
            if ([tourInfoDic[@"hotel_name"] isKindOfClass:[NSString class]])
            InfataLabel.text = tourInfoDic[@"hotel_name"];
            [cellqq addSubview:InfataLabel];
            break;}
        case 3:{
            cellqq.infoLabel.text =@"Категория:";
            UIImageView *hotelRatingsImage = [[UIImageView alloc] initWithFrame:CGRectMake(90, 8, 104, 20)];
            
            if ([starsCount isKindOfClass:[NSString class]]) {
                int intValue = [starsCount intValue];
                if (intValue == 0)
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"zero_stars@2x.png"];
                }
                else if (intValue == 1)
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"one_star@2x.png"];
                }
                else if (intValue == 2)
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"two_stars@2x.png"];
                }
                else if (intValue == 3)
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"three_stars@2x.png"];
                }
                else if (intValue == 4)
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"four_stars@2x.png"];
                }
                else
                {
                    hotelRatingsImage.image = [UIImage imageNamed:@"five_stars@2x.png"];
                }
            }
            [cellqq addSubview:hotelRatingsImage];
            break;}
//        case 4:{
//            cellqq.infoLabel.text =@"Номер в отеле:";
//            InfataLabel.frame=CGRectMake(111, 10, 280, 20);
//            if ([tourInfoDic[@"hotel_number"] isKindOfClass:[NSString class]])
//            InfataLabel.text = tourInfoDic[@"hotel_number"];
//            [cellqq addSubview:InfataLabel];
//            break;}
//        case 5:{
//            cellqq.infoLabel.text =@"Тип питания:";
//            InfataLabel.frame=CGRectMake(95, 10, 280, 20);
//            if ([tourInfoDic[@"hotel_food"] isKindOfClass:[NSString class]])
//            InfataLabel.text = tourInfoDic[@"hotel_food"];
//            [cellqq addSubview:InfataLabel];
//            break;}
//        case 6:{
//            cellqq.infoLabel.text =@"Трансфер:";
//            InfataLabel.frame=CGRectMake(82, 10, 280, 20);
//            if ([tourInfoDic[@"hotel_transfer"] isKindOfClass:[NSString class]])
//            InfataLabel.text = tourInfoDic[@"hotel_transfer"];
//            [cellqq addSubview:InfataLabel];
//            break;}
//        case 7:{
//            cellqq.infoLabel.text =@"Наличие авиаперелета:";
//            InfataLabel.frame=CGRectMake(168, 10, 280, 20);
//            //InfataLabel.text = tourInfoDic[@"hotel_flights"];
//            NSLog(@"%@", tourInfoDic[@"hotel_flights"]);
//            [cellqq addSubview:InfataLabel];
//            break;}
//        case 8:{
//            cellqq.infoLabel.text =@"Класс авиабилета:";
//            InfataLabel.frame=CGRectMake(135, 10, 280, 20);
//            InfataLabel.text = @"нет в запросе";
//            [cellqq addSubview:InfataLabel];
//            break;}
//        case 7:{
//            cellqq.infoLabel.text =@"Дата заезда:";
//            InfataLabel.frame=CGRectMake(96, 10, 280, 20);
//            if ([tourInfoDic[@"depature_date"] isKindOfClass:[NSString class]])
//            InfataLabel.text = tourInfoDic[@"depature_date"];
//            [cellqq addSubview:InfataLabel];
//            break;}
//        default:{
//            cellqq.infoLabel.text =@"Телефон:";
//            InfataLabel.frame=CGRectMake(74, 10, 280, 20);
//            if ([tourInfoDic[@"hotel_telephone"] isKindOfClass:[NSString class]])
//            InfataLabel.text = tourInfoDic[@"hotel_telephone"];
//            [cellqq addSubview:InfataLabel];
//            break;}
//        default:{
//            cellqq.infoLabel.text =@"Туроператор:";
//            InfataLabel.frame=CGRectMake(100, 10, 280, 20);
//            NSLog(@"%@", tourInfoDic[@"hotel_touroperator"]);
//            //InfataLabel.text = tourInfoDic[@"hotel_touroperator"];
//            [cellqq addSubview:InfataLabel];
//            break;}
    }
    [cellqq addSubview:cellqq.infoLabel];
    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    if (indexPath.row==0) {
        backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"top.png"]];
    }else if (indexPath.row==3){
        backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bottom.png"]];
    }else{
        backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mibble.png"]];
    }    
    cellqq.backgroundView = backView;
        
    }else{
        cellqq.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cellqq.selectionStyle=UITableViewCellSelectionStyleNone;
        UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
        if (indexPath.row==0) {
            backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"top.png"]];
        }else if (indexPath.row==8){
            backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bottom.png"]];
        }else{
            backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mibble.png"]];
        }
        cellqq.backgroundView = backView;
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(38, 9, 280, 20)];
        label.font = [UIFont boldSystemFontOfSize:13];
        label.backgroundColor = [UIColor clearColor];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, 20, 20)];
        switch (indexPath.row)
            {
            case 0:
                    imageView.image=[UIImage imageNamed:@"meg.png"];
                    label.text=@"Описание отеля";
                break;
            case 1:
                imageView.image= [UIImage imageNamed:@"pin.png"];
                    label.text=@"Расположение";
                break;
            case 2:
                imageView.image= [UIImage imageNamed:@"flat.png"];
                    label.text=@"В номере";
                break;
            case 3:
                imageView.image= [UIImage imageNamed:@"hotel.png"];
                    label.text=@"Сервис в отеле";
                break;
            case 4:
                imageView.image= [UIImage imageNamed:@"coctail.png"];
                    label.text=@"Рестораны и бары";
                break;
            case 5:
                imageView.image= [UIImage imageNamed:@"dan.png"];
                    label.text=@"Развлечения и спорт";
                break;
            case 6:
                imageView.image= [UIImage imageNamed:@"sea.png"];
                    label.text=@"Пляж";
                break;
            case 7:
                imageView.image= [UIImage imageNamed:@"money.png"];
                    label.text=@"К оплате";
                break;
            default:
                imageView.image= [UIImage imageNamed:@"com.png"];
                    label.text=@"Комментарии туроператора";
                break;
            }
        [cellqq addSubview:imageView];
        [cellqq addSubview:label];
    }
    return cellqq;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == secondTourInfo) {
        DescriptionTourTextViewController *descriptionTourTextViewController = [[DescriptionTourTextViewController alloc] initWithNibName:@"DescriptionTourTextViewController" bundle:nil];
        descriptionTourTextViewController.delegate = self;
        descriptionTourTextViewController.stringTextView = [tourInfoArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:descriptionTourTextViewController animated:YES];
    }

}


@end
