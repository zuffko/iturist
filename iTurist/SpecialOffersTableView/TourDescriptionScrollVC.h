//
//  DescriptionScrolViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"

#import "DescriptionTourTextViewController.h"

@interface TourDescriptionScrollVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableData *receivedData;
    UITableView *firstTourInfo;
    NSMutableArray *tourInfoArray;
    NSMutableDictionary *tourInfoDic;
}

@property (weak, nonatomic) IBOutlet UIScrollView *photoScrollView;
@property (nonatomic, retain) NSString *tourId;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *firstTourInfo;
@property (strong, nonatomic) IBOutlet UITableView *secondTourInfo;

@property (nonatomic, retain) id delegate;

@property (strong, nonatomic) IBOutlet UILabel *nameTourDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *stTourDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *peopleNightDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyDescriptionLabel;

@property (strong, nonatomic) IBOutlet UIImageView *prezentationDescriptionImageView;
@property (strong, nonatomic) IBOutlet UIImageView *secondDescriptionImageView;

@property (nonatomic, retain) NSString       *starsCount;
@property (nonatomic, retain) NSString       *nameTourDescriptionLabelString;
@property (nonatomic, retain) NSString       *peopleNightDescriptionLabelString;
@property (nonatomic, retain) NSString       *moneyDescriptionLabelString;
@property (nonatomic, retain) NSURL          *imageUrl;

@property (nonatomic, retain) NSMutableArray *slidesUrlArray;
@property (nonatomic, retain) NSMutableArray *slidesPhotosArray;
@property (nonatomic, retain) NSMutableArray *tourInfoArray;

@end
