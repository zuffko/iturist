//
//  TourDescriptionCell.h
//  iTurist
//
//  Created by zufa on 2/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourDescriptionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
+(TourDescriptionCell*)cell;
+(NSString *)cellID;
@end
