//
//  TourDescriptionCell.m
//  iTurist
//
//  Created by zufa on 2/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "TourDescriptionCell.h"

@implementation TourDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

+(TourDescriptionCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TourDescriptionCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"TourDescriptionCell";
}


@end
