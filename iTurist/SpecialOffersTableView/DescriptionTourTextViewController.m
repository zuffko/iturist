//
//  DescriptionTourTextViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "DescriptionTourTextViewController.h"

@interface DescriptionTourTextViewController ()

@end

@implementation DescriptionTourTextViewController

@synthesize textView;
@synthesize stringTextView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    self.textView.backgroundColor = [UIColor clearColor];
    if ([stringTextView isKindOfClass:[NSString class]]) {
        self.textView.text = [stringTextView stringByReplacingOccurrencesOfString:@"&quot;" withString:@""];
        
        NSLog(@"%@", stringTextView);
    }else{
        self.textView.text = @"нет информации";
    }
}

@end
