//
//  NSMutableDictionary+DeleteNill.m
//  iTurist
//
//  Created by zufa on 19/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "NSMutableDictionary+DeleteNill.h"

@implementation NSMutableDictionary (DeleteNill)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings {
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for(NSString *key in self) {
        const id object = [self objectForKey:key];
        if(object == nul) {
            //pointer comparison is way faster than -isKindOfClass:
            //since [NSNull null] is a singleton, they'll all point to the same
            //location in memory.
            [replaced setObject:blank
                         forKey:key];
        }
    }
    
    return [replaced copy];
}

@end