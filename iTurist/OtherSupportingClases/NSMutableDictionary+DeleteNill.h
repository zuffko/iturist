//
//  NSMutableDictionary+DeleteNill.h
//  iTurist
//
//  Created by zufa on 19/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (DeleteNill)
- (NSDictionary *)dictionaryByReplacingNullsWithStrings;
@end
