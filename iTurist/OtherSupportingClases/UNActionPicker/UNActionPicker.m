//
//  UNActionPicker.m
//  iHomeMoney
//
//  Created by Eugene Romanishin on 23.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UNActionPicker.h"

@implementation UNActionPicker

@synthesize picker;
@synthesize items;
@synthesize delegate;

- (id)initWithItems:(NSArray*)pickerItems title:(NSString*)title {
    self = [super initWithTitle:title delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    if (self) {
        self.items = pickerItems;
        
        CGRect pickerFrame = CGRectMake(0.0f, 30.0f, 320.0f, 430.0f);
        
        self.picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
        picker.showsSelectionIndicator = YES;
        picker.dataSource = self;
        picker.delegate = self;
        
        [self addSubview:picker];
    }
    return self;
}

- (void)showInView:(UIView *)view {
    [super showInView:view];
    [self setBounds:CGRectMake(0.0f, 0.0f, 320.0f, 430.0f)];
}

- (void)setCloseButtonTitle:(NSString*)title color:(UIColor*)color {
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:title]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(250.0f, 30.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = color;
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
    [self addSubview:closeButton];
}

- (void)dismissActionSheet {
    if ([delegate respondsToSelector:@selector(didSelectItem:)]) {
        [delegate performSelector:@selector(didSelectItem:) withObject:[items objectAtIndex:[picker selectedRowInComponent:0]]];
    }
    
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - PickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView 
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView 
numberOfRowsInComponent:(NSInteger)component 
{
    return items.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView 
             titleForRow:(NSInteger)row 
            forComponent:(NSInteger)component 
{
    return [items objectAtIndex:row];
}

@end
