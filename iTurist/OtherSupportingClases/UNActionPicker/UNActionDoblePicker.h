//
//  UNActionPicker.h
//  iHomeMoney
//
//  Created by Eugene Romanishin on 23.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UNActionDoblePicker : UIActionSheet <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) UIPickerView *picker;
@property (strong, nonatomic) NSArray *items1;
@property (strong, nonatomic) NSArray *items2;
@property (assign, nonatomic) id delegate;

- (id)initWithItems:(NSArray*)pickerItems title:(NSString*)title;
- (void)setCloseButtonTitle:(NSString*)title color:(UIColor*)color;

@end
