//
//  OrderTourOrAuctionVC.m
//  iTurist
//
//  Created by zufa on 11/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "OrderTourOrAuctionVC.h"
#import "SBJson.h"
#import "SBJsonWriter.h"

@interface OrderTourOrAuctionVC (){
        NSMutableData *receivedData;
}

@end

@implementation OrderTourOrAuctionVC
@synthesize tourId;
@synthesize infodic;


- (IBAction)orderTour:(id)sender {
    if ([_nameTextField.text length]==0||[_emailTextField.text length]==0||[_telephoneTextField.text length]==0) {
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Заказ тура"message:@"Заполните все поля" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }else{
        if (infodic) {
            [self sendRequestAuction];
        }else{
        [self sendRequestOrder];
        }
    }
}

-(void)allok
{
    UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Заказ тура"message:@"Заявка отправлена" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [myAlert show];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    _nameTextField.delegate = self;
    _emailTextField.delegate = self;
    _telephoneTextField.delegate = self;
    _commentTextField.delegate = self;
    
    [self setTextFieldHeight40:_nameTextField];
    [self setTextFieldHeight40:_emailTextField];
    [self setTextFieldHeight40:_telephoneTextField];
    [self setTextFieldHeight40:_commentTextField];
    
    _nameTextField.background =_emailTextField.background = _telephoneTextField.background = _commentTextField.background = [ UIImage imageNamed:@"input1@2x.png"];
}

-(void)setTextFieldHeight40:(UITextField*)textField
{
    CGRect frameRect = textField.frame;
    frameRect.size.height = 40;
    textField.frame = frameRect;
    textField.borderStyle = UITextBorderStyleNone;
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView=leftView;
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
}

-(void)sendRequestOrder
{
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithObject:tourId forKey:@"tourId"];
    [jsonDict setObject:_nameTextField.text forKey:@"name"];
    [jsonDict setObject:_emailTextField.text forKey:@"email"];
    [jsonDict setObject:_telephoneTextField.text forKey:@"phone"];
    [jsonDict setObject:_commentTextField.text forKey:@"comment"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=orderTour&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection)
    {
        NSLog(@"Connecting...");
        receivedData =  [NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)sendRequestAuction
{
    [infodic setObject:_nameTextField.text forKey:@"name"];
    [infodic setObject:_emailTextField.text forKey:@"email"];
    [infodic setObject:_telephoneTextField.text forKey:@"phone"];
    [infodic setObject:_commentTextField.text forKey:@"comment"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:infodic];
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=requestAuction&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection)
    {
        NSLog(@"Connecting...");
        receivedData =  [NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];

    NSDictionary *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
    UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Заказ тура"message:parsedArrayCounry[@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [myAlert show];
    

}


@end
