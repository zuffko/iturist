//
//  OrderTourOrAuctionVC.h
//  iTurist
//
//  Created by zufa on 11/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTourOrAuctionVC : UIViewController <UITextFieldDelegate>

@property (nonatomic, retain) NSString *tourId;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (nonatomic, retain) NSMutableDictionary *infodic;

@end
