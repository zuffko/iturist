//
//  AuctionViewController.m/Users/Gaponov/Documents/XCodeProgect/Kazakstan/iTurist/iTurist/ViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "AuctionViewController.h"
#import "MBProgressHUD.h"
#import "OrderTourOrAuctionVC.h"

@implementation AuctionViewController{
    NSMutableData       *receivedData;
    NSMutableURLRequest *request;
    NSMutableArray      *countryMutableArray;
    NSMutableArray      *cityMutableArray;
    NSMutableArray      *resortMutableArray;
    NSMutableArray      *aurFirmMutableArray;
    UNActionPicker      *actionPicker;
    UIActionSheet       *pickerAction;
    UIDatePicker        *datePicker;
    UIToolbar           *toolbarPicker;
    NSString            *date;
    
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    NSURLConnection *connection3;
    NSURLConnection *connection4;
    int curAct;
    NSMutableDictionary *auctionDIctionary;
    NSMutableDictionary *countyDic;
    NSMutableDictionary *cityDic;
    NSMutableDictionary *resortDic;
    NSMutableDictionary *aurFirmDic;
    NSString *dateIn;
    NSString *dateOut;
    NSString *countyId;
    NSString *cityId;
    NSString *resortId;
    NSString *roomTypeId;
    NSString *hotelClassId;
    NSString *ticketClass;
    NSString *includingAirfare;
    NSString *human;
    NSString *kidTo12;
    NSString *kidTo2;
    NSString *airFirmID;
    NSMutableDictionary*infodic;
    
}

@synthesize butonCountry;
@synthesize butonCity;
@synthesize butonHotel;
@synthesize butonRoom;
@synthesize butonClasHotel;
@synthesize buttonDateIn;
@synthesize buttonDateOut;
@synthesize buttonHumanCount;
@synthesize buttonKidTo12;
@synthesize buttonKidTo2;
@synthesize buttonAirline;
@synthesize buttonTicketClass;

- (void)viewDidLoad{
    [super viewDidLoad];
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Аукцион";
    self.navigationItem.titleView = label;
    self.navigationItem.titleView=label;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]];
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://iturist.kz/mobile.php?methodName=getCountries"]
                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection1)
    {
        NSLog(@"Connecting...");
        receivedData =[NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
    
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://iturist.kz/mobile.php?methodName=getAirlineCompanies"]
                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    connection4 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection1)
    {
        NSLog(@"Connecting...");
        receivedData =[NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    _scrollView.delegate = self;
    _budgetMin.delegate=self;
    _budgetMax.delegate=self;
    
    [self setTextFieldHeight40:_budgetMin];
    [self setTextFieldHeight40:_budgetMax];

    _budgetMin.background = _budgetMax.background = [ UIImage imageNamed:@"input_price@2x.png"];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [_scrollView addGestureRecognizer:singleTap];
}

-(void)setTextFieldHeight40:(UITextField*)textField{
    CGRect frameRect = textField.frame;
    frameRect.size.height = 40;
    textField.frame = frameRect;
    textField.borderStyle = UITextBorderStyleNone;
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView=leftView;
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if (connection==connection1) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
        NSDictionary  *dictionary = [NSDictionary new];
        countryMutableArray= [[NSMutableArray alloc] init];
        int i;
        countyDic=[NSMutableDictionary new];
        
        for (i=0; i<=[parsedArrayCounry count]-1; i++)
        {
            dictionary = [parsedArrayCounry objectAtIndex:i];
            if (dictionary != nil)
            {
                [countryMutableArray addObject:[dictionary objectForKey:@"name"]];
                [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                [countyDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
            }
        }
    } else if (connection==connection2){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
        NSDictionary  *dictionary = [NSDictionary new];
        cityMutableArray= [[NSMutableArray alloc] init];
        int i;
        cityDic=[NSMutableDictionary new];
        
        if ([parsedArrayCounry count]==0) {
            UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Список городов"message:@"В данной стране нет городов" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [myAlert show];
        }else{
            for (i=0; i<=[parsedArrayCounry count]-1; i++)
            {
                dictionary = [parsedArrayCounry objectAtIndex:i];
                if (dictionary != nil)
                {
                    [cityMutableArray addObject:[dictionary objectForKey:@"name"]];
                    [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                    [cityDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
                }
            }
        }
        NSLog(@"%@", cityDic);
    }   else if (connection==connection3){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
        if (parsedArrayCounry == (id)[NSNull null]) {
            UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Список отелей"message:@"В данном городе нет доступных отелей" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [myAlert show];
        }else{
        NSDictionary  *dictionary = [NSDictionary new];
        resortMutableArray= [[NSMutableArray alloc] init];
        int i;
        resortDic=[NSMutableDictionary new];
        
        for (i=0; i<=[parsedArrayCounry count]-1; i++)
        {
            dictionary = [parsedArrayCounry objectAtIndex:i];
            if (dictionary != nil)
            {
                [resortMutableArray addObject:[dictionary objectForKey:@"name"]];
                [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                [resortDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
            }
        }
        NSLog(@"%@", resortDic);
    }
    } else  if (connection==connection4) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
            NSDictionary  *dictionary = [NSDictionary new];
            aurFirmMutableArray= [[NSMutableArray alloc] init];
            int i;
            aurFirmDic=[NSMutableDictionary new];
            
            for (i=0; i<=[parsedArrayCounry count]-1; i++)
            {
                dictionary = [parsedArrayCounry objectAtIndex:i];
                if (dictionary != nil)
                {
                    [aurFirmMutableArray addObject:[dictionary objectForKey:@"name"]];
                    [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                    [aurFirmDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
                }
            }
        NSLog(@"%@", aurFirmDic);
    }
}

- (IBAction)butonCountry:(id)sender{
    if (![countryMutableArray count]==0)
    [self showActionPickerWithTitle:@"Страна" andDataSourse:countryMutableArray];
    curAct=1;
}

- (IBAction)butonCity:(id)sender{
    if (![cityMutableArray count]==0){
    [self showActionPickerWithTitle:@"Город/Курорт" andDataSourse:cityMutableArray];
    }else{
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Город/Курорт"message:@"Вы не выбрали страну или данные для этой страны отсутствуют" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }
    curAct=2;
}

- (IBAction)butonHotel:(id)sender{
    if (![resortMutableArray count]==0){
    [self showActionPickerWithTitle:@"Отель" andDataSourse:resortMutableArray];
    }else{
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Отель"message:@"Вы не выбрали город или данные для этого города отсутствуют" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }
    curAct=3;
}

- (IBAction)butonRoom:(id)sender{
    [self showActionPickerWithTitle:@"Номер в отеле" andDataSourse:[NSArray arrayWithObjects:@"Стандарт",@"Полулюкс",@"Люкс",@"Вилла",@"Другой", nil]];
    curAct=4;
}

- (IBAction)butonClasHotel:(id)sende{
    [self showActionPickerWithTitle:@"Клас отеля" andDataSourse:[NSArray arrayWithObjects:@"2 класс", @"3 класс",@"4 класс",@"5 класс",@"другой", nil]];
    curAct=5;
}

- (IBAction)dateIn:(id)sender {
    curAct=10;
    [self showDatePicker];
}

- (IBAction)dateOut:(id)sender {
    curAct=11;
    [self showDatePicker];
}

- (IBAction)numanCount:(id)sender {
    [self showActionPickerWithTitle:@"Количество взрослых:" andDataSourse:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil]];
    curAct=7;
}

- (IBAction)humanTo12Count:(id)sender {
    [self showActionPickerWithTitle:@"Количество детей до 12-ти лет:" andDataSourse:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil]];
    curAct=8;
}

- (IBAction)humanTo2Count:(id)sender {
    [self showActionPickerWithTitle:@"Количество детей до 2-х лет:" andDataSourse:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil]];
    curAct=9;
}

- (IBAction)presenceOfFlight:(id)sender {
    [self showActionPickerWithTitle:@"Наличие авиаперелета" andDataSourse:[NSArray arrayWithObjects:@"С перелетом", @"Без перелета", nil]];
    curAct=6;
}

- (IBAction)ticketClass:(id)sender {
    [self showActionPickerWithTitle:@"Класс билета" andDataSourse:[NSArray arrayWithObjects:@"Не требуется", @"Бизнес", @"Эконом", @"Другой", nil]];
    curAct =10;
}

- (IBAction)airFirm:(id)sender {
        [self showActionPickerWithTitle:@"Авиаперевозчик" andDataSourse: aurFirmMutableArray];
    curAct =11;
}

- (IBAction)transfer:(id)sender {
    [self showActionPickerWithTitle:@"Номер в отеле" andDataSourse:[NSArray arrayWithObjects:@"Индивидуальный",@"Не требуется", nil]];
    curAct=12;
}

- (IBAction)sold:(id)sender {
    infodic = [NSMutableDictionary new];
    if (countyId==nil||_budgetMin.text==nil||_budgetMax.text==nil||human==nil) {
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Аукцион"message:@"Заполните обязательные поля: бюджет, страна, количество взрозслых" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }else{
        if (countyId==nil) {
            countyId=@" ";
        }
        if (cityId==nil) {
            cityId=@" ";
        }
        if (roomTypeId==nil) {
            roomTypeId=@" ";
        }
        if (hotelClassId==nil) {
            hotelClassId=@" ";
        }
        if (_budgetMin.text==nil) {
            _budgetMin.text=@" ";
        }
        if (_budgetMax.text==nil) {
            _budgetMax.text=@" ";
        }
        if (human==nil) {
            human=@" ";
        }
        if (kidTo12==nil) {
            kidTo12=@" ";
        }
        if (kidTo2==nil) {
            kidTo2=@" ";
        }
        if (airFirmID==nil) {
            airFirmID=@" ";
        }
        if (ticketClass==nil) {
            ticketClass=@" ";
        }
        if (includingAirfare==nil) {
            includingAirfare=@" ";
        }
        if (dateIn==nil) {
            dateIn=@" ";
        }
        if (dateOut==nil) {
            dateOut=@" ";
        }
        
        if (_daysSwitch.on) {
            [infodic setObject:@"1" forKey:@"date_check_in_three_days_rang"];
        }else{
            [infodic setObject:@"0" forKey:@"date_check_in_three_days_rang"];
        }
        
        [infodic setObject:countyId forKey:@"country_id"];
        [infodic setObject:cityId forKey:@"resort_id"];
        [infodic setObject:roomTypeId forKey:@"room_type_id"];
        [infodic setObject:hotelClassId forKey:@"hotel_class_id"];
        [infodic setObject:_budgetMin.text forKey:@"budget_min"];
        [infodic setObject:_budgetMax.text forKey:@"budget_max"];
        [infodic setObject:dateIn forKey:@"date_check_in"];
        [infodic setObject:dateOut forKey:@"date_check_out"];
        [infodic setObject:human forKey:@"adults_qty"];
        [infodic setObject:kidTo12 forKey:@"teens_qty"];
        [infodic setObject:kidTo2 forKey:@"babys_qty"];
        [infodic setObject:airFirmID forKey:@"airline_company_id"];
        [infodic setObject:ticketClass forKey:@"air_ticket_class_id"];
        [infodic setObject:includingAirfare forKey:@"including_airfare"];
        [self performSegueWithIdentifier:@"sold" sender:nil];
        NSLog(@"%@", infodic);
    }
}

- (void)didSelectItem:(id)item{
    NSLog(@"%@", item);
    
    if (curAct==1) {
        [self addLabelWithText:item inButton:butonCountry];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        countyId = countyDic[item];
        NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:countyDic[item] forKey:@"country_id"];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getCities&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        connection2 = [[NSURLConnection alloc] initWithRequest:request1 delegate:self];
        if (connection2)
        {
            NSLog(@"Connecting...");
            receivedData =[NSMutableData data];
        }
        else
        {
            NSLog(@"Connection error!");
        }
    }else if (curAct==2){
        [self addLabelWithText:item inButton:butonCity];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        cityId = cityDic[item];
        NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:cityDic[item] forKey:@"resort_id"];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getHotels&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"%@", url);
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        connection3 = [[NSURLConnection alloc] initWithRequest:request1 delegate:self];
        if (connection3)
        {
            NSLog(@"Connecting...");
            receivedData =[NSMutableData data];
        }
        else
        {
            NSLog(@"Connection error!");
        }
    } else if (curAct == 3){
        [self addLabelWithText:item inButton:butonHotel];
        if (item) {
            resortId=resortDic[item];
        }
    }else if (curAct==4){
        [self addLabelWithText:item inButton:butonRoom];
        if ([item isEqual:@"Стандарт"]){
            roomTypeId=@"0";
        }else if ([item isEqual:@"Полулюкс"]){
            roomTypeId=@"1";
        }else if ([item isEqual:@"Люкс"]){
            roomTypeId=@"2";
        }else if ([item isEqual:@"Вилла"]){
            roomTypeId=@"3";
        }
    }else if (curAct==5){
        [self addLabelWithText:item inButton:butonClasHotel];
        if ([item isEqual:@"2 класс"]){
            hotelClassId=@"0";
        }else if ([item isEqual:@"3 класс"]){
            hotelClassId=@"1";
        }else if ([item isEqual:@"4 класс"]){
            hotelClassId=@"2";
        }else if ([item isEqual:@"5 класс"]){
            hotelClassId=@"3";
        }else if ([item isEqual:@"другой"]){
            hotelClassId=@"4";
        }
    }else if (curAct==6){
        [self addLabelWithText:item inButton:buttonAirline];
        if ([item isEqual:@"С перелетом"]){
            includingAirfare=@"0";
        }else if ([item isEqual:@"Без перелета"]){
            includingAirfare=@"1";
        }
    }else if (curAct==7){
        [self addLabelWithText:item inButton:buttonHumanCount];
        human=item;
    }else if (curAct==8){
        [self addLabelWithText:item inButton:buttonKidTo12];
        kidTo12=item;
    }else if (curAct==9){
        [self addLabelWithText:item inButton:buttonKidTo2];
        kidTo2=item;
    }else if (curAct==10){
        [self addLabelWithText:item inButton:buttonTicketClass];
        if ([item isEqual:@"Не требуется"]){
            ticketClass=@"0";
        }else if ([item isEqual:@"Бизнес"]){
            ticketClass=@"1";
        }else if ([item isEqual:@"Эконом"]){
            ticketClass=@"2";
        }else if ([item isEqual:@"5 класс"]){
            ticketClass=@"3";
        }else if ([item isEqual:@"Другой"]){
            ticketClass=@"4";
        }
        
    }else if (curAct==11){
        [self addLabelWithText:item inButton:_buttonAirFirm];
        if (item) {
            airFirmID=aurFirmDic[item];
        }
    }else if (curAct==12){
        [self addLabelWithText:item inButton:_buttonTransfer];
    }
    curAct=0;
}

-(void)addLabelWithText:(NSString*)text inButton:(UIButton*)button{
    for (UIView *subview in [button subviews]) {
        if ([subview isKindOfClass:[UILabel class]]) {
            [subview removeFromSuperview];
        }
    }
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 280, 20)];
    label.text=text;
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    [button addSubview:label];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"sold"])
    {
        OrderTourOrAuctionVC *vc = [segue destinationViewController];
        vc.infodic=infodic;
    }
}

-(void)showDatePicker{
    pickerAction = [[UIActionSheet alloc] initWithTitle:@"Date"
                                               delegate:nil
                                      cancelButtonTitle:nil
                                 destructiveButtonTitle:nil
                                      otherButtonTitles:nil];
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake ( 0.0, 44.0, 0.0, 0.0)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    //format datePicker mode. in this example time is used
    datePicker.datePickerMode = UIDatePickerModeDate;
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    //calls dateChanged when value of picker is changed
    //[datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    toolbarPicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbarPicker.barStyle=UIBarStyleBlackOpaque;
    [toolbarPicker sizeToFit];
    NSMutableArray *itemsBar = [[NSMutableArray alloc] init];
    //calls DoneClicked
    UIBarButtonItem *bbitem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneClicked)];
    [itemsBar addObject:bbitem];
    [toolbarPicker setItems:itemsBar animated:YES];
    [pickerAction addSubview:toolbarPicker];
    [pickerAction addSubview:datePicker];
    [pickerAction showInView:self.view];
    [pickerAction setBounds:CGRectMake(0,0,320, 464)];
}

-(BOOL)closeDatePicker:(id)sender{
    [pickerAction dismissWithClickedButtonIndex:0 animated:YES];
    //[date resignFirstResponder];
    
    return YES;
}

-(IBAction)DoneClicked{
    [self dateChanged];
    [self closeDatePicker:self];
    datePicker.frame=CGRectMake(0, 44, 320, 416);
}

-(IBAction)dateChanged{
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] ];
    [FormatDate setDateFormat:@"YYYY-MM-dd"];
    date = [FormatDate stringFromDate:[datePicker date]];
    NSLog(@"%@", date);
    if (curAct==10) {
        dateIn=date;
        [self addLabelWithText:date inButton:buttonDateIn];
    }else if (curAct==11){
        dateOut=date;
        [self addLabelWithText:date inButton:buttonDateOut];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    NSLog(@"%@", errorString);
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 40;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)showActionPickerWithTitle:(NSString *)title andDataSourse:(NSArray *)dataSourseArray{
    actionPicker = [[UNActionPicker alloc] initWithItems:dataSourseArray title:title];
    [actionPicker setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionPicker setCloseButtonTitle:@"Ok" color:[UIColor blackColor]];
    actionPicker.delegate = self;
    [actionPicker showInView:self.view];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.view endEditing:YES];
}

@end
