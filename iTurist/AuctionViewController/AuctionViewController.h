//
//  AuctionViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"
#import "UNActionPicker.h"

@interface AuctionViewController : UIViewController <UIScrollViewDelegate, UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UISwitch *daysSwitch;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIButton  *butonCountry;
@property (nonatomic, retain) IBOutlet UIButton  *butonCity;
@property (nonatomic, retain) IBOutlet UIButton  *butonHotel;
@property (nonatomic, retain) IBOutlet UIButton  *butonRoom;
@property (nonatomic, retain) IBOutlet UIButton  *butonClasHotel;
@property (weak, nonatomic) IBOutlet UIButton *buttonDateIn;
@property (weak, nonatomic) IBOutlet UIButton *buttonDateOut;
@property (weak, nonatomic) IBOutlet UIButton *buttonHumanCount;
@property (weak, nonatomic) IBOutlet UIButton *buttonKidTo12;
@property (weak, nonatomic) IBOutlet UIButton *buttonKidTo2;
@property (weak, nonatomic) IBOutlet UIButton *buttonAirline;
@property (weak, nonatomic) IBOutlet UIButton *buttonTicketClass;
@property (weak, nonatomic) IBOutlet UIButton *buttonAirFirm;
@property (weak, nonatomic) IBOutlet UITextField *budgetMin;
@property (weak, nonatomic) IBOutlet UITextField *budgetMax;
@property (weak, nonatomic) IBOutlet UIButton *buttonTransfer;
@end


