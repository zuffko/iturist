//
//  TextViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "TextViewController.h"

@interface TextViewController ()

@end

@implementation TextViewController

@synthesize textView;
@synthesize arrayTextView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
//    VisasDescriptionViewController *visasDescriptionViewController = [[VisasDescriptionViewController alloc] initWithNibName:@"VisasDescriptionViewController" bundle:nil];
    
//    self.arrayTextView = visasDescriptionViewController.array1;
    
    self.textView.text = [NSString stringWithFormat:@"%@",arrayTextView];
    NSLog(@"%@", arrayTextView);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
