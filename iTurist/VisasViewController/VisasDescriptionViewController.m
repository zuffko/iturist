//
//  VisasDescriptionViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "VisasDescriptionViewController.h"

@interface VisasDescriptionViewController ()

@end

@implementation VisasDescriptionViewController
@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize conditions;
@synthesize documents;
@synthesize price;
@synthesize terms;
@synthesize urlString;
@synthesize dictionaryRequest;
@synthesize countryId;
@synthesize completeUrlString;
@synthesize request;
@synthesize connection;
@synthesize  errorString;
@synthesize response;
@synthesize arrayRequest;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self requests];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
}

- (void)requests
{
    //urlString = @"http://iturist.kz/mobile.php?methodName=getCountryVisaInfo&json=";
    
    //completeUrlString = [NSString stringWithFormat:@"%@%@", urlString, countryId];
    
    
    NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:countryId forKey:@"country_id"];
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getCountryVisaInfo&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection)
    {
        NSLog(@"Connecting...");
        receivedData =  [NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }

    
    
    
    
    
//    request = [NSURLRequest requestWithURL:[NSURL URLWithString:completeUrlString]
//                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
//    
//    NSLog(@"%@", request);
//    
//    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    NSLog(@"%@", connection);
//    
//    if (connection)
//    {
//        NSLog(@"Connecting...");
//        
//        receivedData =  [NSMutableData data];
//    }
//    else
//    {
//        NSLog(@"Connection error!");
//    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                   [error localizedDescription],
                   [error description],
                   [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    arrayRequest = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
    for (int i = 0; i<=[arrayRequest count]-1; i++)
    {
        dictionaryRequest = [arrayRequest objectAtIndex:i];
        conditions = [dictionaryRequest objectForKey:@"conditions"];
        documents = [dictionaryRequest objectForKey:@"documents"];
        price = [dictionaryRequest objectForKey:@"price"];
        terms = [dictionaryRequest objectForKey:@"terms"];
    }
}

- (IBAction)button1:(id)sender
{
    TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController" bundle:nil];
    textViewController.delegate = self;
    textViewController.arrayTextView = conditions;
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (IBAction)button2:(id)sender
{
    TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController" bundle:nil];
    textViewController.delegate = self;
    textViewController.arrayTextView = terms;
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (IBAction)button3:(id)sender
{
    TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController" bundle:nil];
    textViewController.delegate = self;
    textViewController.arrayTextView = documents;
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (IBAction)button4:(id)sender
{
    TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController" bundle:nil];
    textViewController.delegate = self;
    textViewController.arrayTextView = price;
    [self.navigationController pushViewController:textViewController animated:YES];
}

@end
