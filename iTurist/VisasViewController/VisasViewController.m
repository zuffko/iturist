//
//  VisasViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "VisasViewController.h"
#import "MBProgressHUD.h"


#define titleVisaYes @"С визовым режимом"
#define titleVisaNo @"С безвизовым режимом"

@interface VisasViewController ()

@end

@implementation VisasViewController

@synthesize visas;

- (NSArray*)curentCountry:(NSInteger)index
{
    NSArray *keys = [visas allKeys];
    NSString *curentKey = [keys objectAtIndex:index];
    NSArray *curentCountry = [visas objectForKey:curentKey];
    return curentCountry;
}


@synthesize JSONresponse;
@synthesize response;

@synthesize dictionaryGetCountry;
@synthesize arrayToursGetCountry;

@synthesize getCountriesRequest;
@synthesize arrayGetCountry;

@synthesize request;

@synthesize arrayVisaNo;
@synthesize countriesVisaNo;

@synthesize arrayVisaYes;
@synthesize countriesVisaYes;
@synthesize getCountriesVisaYes;
@synthesize arrayCountryIdVisaYes;
@synthesize countriesIdVisaYes;


@synthesize idCountryVisa;
@synthesize needVisasCountry;
@synthesize nameCountryVisaYesNo;
@synthesize getCountriesVisaNo;

@synthesize getCountryVisaInfoRequest;
@synthesize dictionaryGetCountryVisaInfo;
@synthesize arrayToursGetCountryVisaInfo;

@synthesize documentsVisaInfo;
@synthesize priceVisaInfo;
@synthesize conditionsVisaInfo;
@synthesize termsVisaInfo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Визы";
    self.navigationItem.titleView = label;
    self.navigationItem.titleView=label;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    getCountriesRequest = [NSURLRequest requestWithURL:[NSURL
                                                        URLWithString:@"http://iturist.kz/mobile.php?methodName=getCountries"]
                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                       timeoutInterval:15.0];    
    NSLog(@"%@", getCountriesRequest);
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:getCountriesRequest delegate:self];
    NSLog(@"%@", connection);
    
    if (connection)
    {
        NSLog(@"Connecting...");
        receivedData =[NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
    _getCountriesVisaNoToTableView.backgroundColor = [UIColor clearColor];
//    _getCountriesVisaNoToTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _getCountriesVisaNoToTableView.sectionIndexColor=[UIColor clearColor];
    
    
}
#pragma mark - getCountries

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *dataStringGetCountry = [[NSString alloc] initWithData:receivedData
                                                           encoding:NSUTF8StringEncoding];
    NSLog(@"dataStringGetCountry - %@", dataStringGetCountry);
    
    arrayToursGetCountry = [[[[SBJsonParser alloc] init] objectWithString:dataStringGetCountry] objectForKey:@"response"];
    
    NSLog(@"arrayToursGetCountry - %@", arrayToursGetCountry);
    
    arrayGetCountry = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<=[arrayToursGetCountry count]-1; i++)
    {
        dictionaryGetCountry = [arrayToursGetCountry objectAtIndex:i];
        NSLog(@"dictionaryGetCountry - %@", dictionaryGetCountry);

        [arrayGetCountry addObject:dictionaryGetCountry];
        NSLog(@"arrayGetCountry - %@", arrayGetCountry);
    }
    [self createTableView];
    [_getCountriesVisaNoToTableView reloadData];
}

#pragma mark - createTableView

- (void)createTableView
{
    self.title = @"Визы";
    
    arrayVisaNo = [[NSMutableArray alloc] init];
    arrayVisaYes = [[NSMutableArray alloc] init];
    
    arrayCountryIdVisaYes = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<=[arrayGetCountry count]-1; i++)
    {
        dictionaryGetCountry = [arrayGetCountry objectAtIndex:i];
        NSLog(@"dictionaryGetCountry - %@", dictionaryGetCountry);
        
        nameCountryVisaYesNo = [dictionaryGetCountry objectForKey:@"needVisa"];
        int intValue = [nameCountryVisaYesNo intValue];
        NSLog(@"nameCountryVisaYesNo - %@", nameCountryVisaYesNo);
        
        if (intValue == 0)
        {
            countriesVisaNo = [dictionaryGetCountry objectForKey:@"name"];
            NSLog(@"countriesVisaNo - %@", countriesVisaNo);
            
            [arrayVisaNo addObject:countriesVisaNo];
            NSLog(@"arrayVisaNo - %@", arrayVisaNo);
        }
        else if (intValue == 1)
        {
            countriesVisaYes = [dictionaryGetCountry objectForKey:@"name"];
            NSLog(@"countriesVisaYes - %@", countriesVisaYes);

            countriesIdVisaYes = [dictionaryGetCountry objectForKey:@"id"];
            NSLog(@"countriesVisaYes - %@", countriesIdVisaYes);
            
            [arrayVisaYes addObject:countriesVisaYes];
            NSLog(@"arrayVisaYes - %@", arrayVisaYes);
            
            [arrayCountryIdVisaYes addObject:countriesIdVisaYes];
            NSLog(@"arrayCountryIdVisaYes - %@", arrayCountryIdVisaYes);            
        }
    }
    getCountriesVisaNo = [NSArray arrayWithArray:arrayVisaNo];
    NSLog(@"getCountriesVisaNo - %@", getCountriesVisaNo);
    
    getCountriesVisaYes = [NSArray arrayWithArray:arrayVisaYes];
    NSLog(@"getCountriesVisaNo - %@", getCountriesVisaYes);
    
    self.visas = [NSDictionary dictionaryWithObjectsAndKeys:getCountriesVisaYes, titleVisaYes, getCountriesVisaNo, titleVisaNo, nil];
    
    _getCountriesVisaNoToTableView.dataSource = self;
    _getCountriesVisaNoToTableView.delegate = self;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [visas count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *curentCountry = [self curentCountry:section];
    return [curentCountry count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{    
    return [[visas allKeys] objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    _curentCountry = [self curentCountry:indexPath.section];
    cell.textLabel.text = [_curentCountry objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    if (indexPath.row==0) {
        backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"top.png"]];
    }else{
        backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mibble.png"]];
    }
    cell.backgroundView = backView;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row<[getCountriesVisaYes count]){
    for (NSString* item in getCountriesVisaYes)
    {
        if ([item isEqualToString:[_curentCountry objectAtIndex:indexPath.row]]){
            VisasDescriptionViewController *visasDescriptionViewController = [[VisasDescriptionViewController alloc] initWithNibName:@"VisasDescriptionViewController" bundle:nil];
        visasDescriptionViewController.delegate = self;
        
        visasDescriptionViewController.countryId = [NSString stringWithFormat:@"%@", [arrayCountryIdVisaYes objectAtIndex:indexPath.row]];
        NSLog(@"%@", visasDescriptionViewController.countryId);
        NSLog(@"countryId - %@", visasDescriptionViewController.countryId);
        [self.navigationController pushViewController:visasDescriptionViewController animated:YES];
    }
    }
}
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
