//
//  VisasDescriptionViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"

#import "TextViewController.h"

@interface VisasDescriptionViewController : UIViewController
{
    NSMutableData *receivedData;
}

@property (nonatomic, retain) IBOutlet UIButton *button1;
@property (nonatomic, retain) IBOutlet UIButton *button2;
@property (nonatomic, retain) IBOutlet UIButton *button3;
@property (nonatomic, retain) IBOutlet UIButton *button4;

- (IBAction)button1:(id)sender;
- (IBAction)button2:(id)sender;
- (IBAction)button3:(id)sender;
- (IBAction)button4:(id)sender;

@property (nonatomic, retain) NSArray *conditions;
@property (nonatomic, retain) NSArray *documents;
@property (nonatomic, retain) NSArray *price;
@property (nonatomic, retain) NSArray *terms;

@property (nonatomic, retain) NSDictionary *dictionaryRequest;

@property (nonatomic, retain) NSDictionary  *response;
@property (nonatomic, retain) NSString      *errorString;

@property (nonatomic, retain) NSArray       *arrayRequest;

@property (nonatomic, retain) NSMutableURLRequest *request;
@property (nonatomic, retain) NSURLConnection *connection;

@property (nonatomic, retain) NSString       *urlString;

@property (nonatomic, retain) id delegate;

- (void)requests;

@property (nonatomic, retain) NSString *countryId;
@property (nonatomic, retain) NSString *completeUrlString;



@end
