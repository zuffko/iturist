//
//  VisasViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"

#import "VisasDescriptionViewController.h"

@interface VisasViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableData   *receivedData;
    
    NSDictionary *visas;
}

@property (nonatomic, retain) NSDictionary  *visas;

@property (nonatomic, retain) NSMutableURLRequest *getCountriesRequest;

@property (nonatomic, retain) NSDictionary  *JSONresponse;
@property (nonatomic, retain) NSDictionary  *response;

@property (nonatomic, retain) NSDictionary  *dictionaryGetCountry;
@property (nonatomic, retain) NSArray       *arrayToursGetCountry;

@property (nonatomic, retain) NSMutableArray       *arrayGetCountry;


@property (nonatomic, retain) NSMutableURLRequest *request;

@property (nonatomic, retain) NSMutableArray    *arrayCountryIdVisaYes;
@property (nonatomic, retain) NSMutableArray    *arrayVisaYes;
@property (nonatomic, retain) NSArray       *countriesVisaYes;
@property (nonatomic, retain) NSArray       *countriesIdVisaYes;

@property (nonatomic, retain) NSMutableArray    *arrayVisaNo;
@property (nonatomic, retain) NSArray       *countriesVisaNo;

@property (nonatomic, retain) NSString      *idCountryVisa; //id – уникальный идентификатор страны (integer)
@property (nonatomic, retain) NSString      *needVisasCountry; //name – наименование страны (text)
@property (nonatomic, retain) NSString      *nameCountryVisaYesNo; //needVisa – для посещения страны нужна виза (integer, 0 – не нужна, 1 – нужна)

@property (nonatomic, retain) IBOutlet UITableView      *getCountriesVisaNoToTableView;
@property (nonatomic, retain) NSArray                   *getCountriesVisaNo;
@property (nonatomic, retain) NSArray                   *getCountriesVisaYes;


@property (nonatomic, retain) NSDictionary *students;



@property (nonatomic, retain) NSDictionary  *dictionaryGetCountryVisaInfo;
@property (nonatomic, retain) NSArray       *arrayToursGetCountryVisaInfo;


@property (nonatomic, retain) NSString      *documentsVisaInfo;
@property (nonatomic, retain) NSString      *priceVisaInfo;
@property (nonatomic, retain) NSString      *conditionsVisaInfo;
@property (nonatomic, retain) NSString      *termsVisaInfo;

@property (nonatomic, retain) NSString      *conditionsCountryVisa;   //conditions – условия получения визы (text)
@property (nonatomic, retain) NSString      *documentsCountryVisa;     //documents – список необходимых документов (text)
@property (nonatomic, retain) NSString      *priceCountryVisa;             //price – стоимость получения визы (text)
@property (nonatomic, retain) NSString      *termsCountryVisa;             //terms – сроки исполнения (text)

@property (nonatomic, retain) NSMutableURLRequest *getCountryVisaInfoRequest;

@property (nonatomic, retain) NSArray *curentCountry;
@end
