//
//  TextViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 27.06.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewController.h"
#import "VisasDescriptionViewController.h"

@interface TextViewController : UIViewController

@property (nonatomic, retain) id delegate;

@property (nonatomic, retain) IBOutlet UITextView *textView;

@property (nonatomic, retain) NSArray *arrayTextView;

@end
