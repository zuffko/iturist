//
//  SearchVC.m
//  iTurist
//
//  Created by zufa on 9/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "SearchVC.h"
#import "SpecialOffersViewController.h"
#import "MBProgressHUD.h"

@interface SearchVC ()
@end

@implementation SearchVC{
    NSMutableArray *countryMutableArray;
    NSMutableArray *sityMutableArray;
    UNActionPicker      *actionPicker;
    UIActionSheet       *pickerAction;
    UIDatePicker        *datePicker;
    UIToolbar           *toolbarPicker;
    NSString            *date;
    NSMutableDictionary *countyDic;
    NSMutableDictionary *cityDic;
    int curAct;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    NSURLConnection *connection3;
    NSString *dateInString;
    NSString *dateOutString;
    NSString *countyId;
    NSString *cityId;
    NSString *budget_min;
    NSString *budget_max;
    NSURL *sendURL;
    NSString *hotCategory;
    NSString *cityName;
    NSString *countyName;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _priceFrom.delegate = self;
    _priceTo.delegate = self;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg-568h.png"]] ;
    [self sendRequest];
    
    [self setTextFieldHeight40:_priceTo];
    [self setTextFieldHeight40:_priceFrom];
    _priceFrom.background = _priceTo.background = [UIImage imageNamed:@"input_price@2x.png"];
    
    [_countyBt setBackgroundColor:[UIColor clearColor]];
    [_cityBt setBackgroundColor:[UIColor clearColor]];
    [_dateInBt setBackgroundColor:[UIColor clearColor]];
    [_dateOutBt setBackgroundColor:[UIColor clearColor]];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_priceFrom) {
        budget_min=_priceFrom.text;
        NSLog(@"%@",_priceFrom.text);
    }else if (textField==_priceTo){
        NSLog(@"%@",_priceTo.text);
        budget_max=_priceTo.text;
    }
}

-(void)setTextFieldHeight40:(UITextField*)textField
{
    CGRect frameRect = textField.frame;
    frameRect.size.height = 40;
    textField.frame = frameRect;
    textField.borderStyle = UITextBorderStyleNone;
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView=leftView;
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
}

-(void)sendRequest
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURL *url = [NSURL URLWithString:@"http://iturist.kz/mobile.php?methodName=getCountries"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection1)
    {
        NSLog(@"Connecting...");
        receivedData =[NSMutableData data];
    }
    else
    {
        NSLog(@"Connection error!");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection==connection1) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
        NSDictionary  *dictionary = [NSDictionary new];
        countryMutableArray= [[NSMutableArray alloc] init];
        int i;
        countyDic=[NSMutableDictionary new];

        for (i=0; i<=[parsedArrayCounry count]-1; i++)
            {
            dictionary = [parsedArrayCounry objectAtIndex:i];
            if (dictionary != nil){
                    [countryMutableArray addObject:[dictionary objectForKey:@"name"]];
                    [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                    [countyDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
                }
            }
    } else if (connection==connection2){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSArray *parsedArrayCounry = [[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"];
        NSDictionary  *dictionary = [NSDictionary new];
        sityMutableArray= [[NSMutableArray alloc] init];
        int i;
        cityDic=[NSMutableDictionary new];
        
        if ([parsedArrayCounry isKindOfClass:[NSNull class]]) {
            UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Список городов"message:@"Данные для этой страны отсутствуют" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [myAlert show];
        }else{
        
        for (i=0; i<=[parsedArrayCounry count]-1; i++)
        {
            dictionary = [parsedArrayCounry objectAtIndex:i];
            if (dictionary != nil)
            {
                [sityMutableArray addObject:[dictionary objectForKey:@"name"]];
                [NSString stringWithFormat:@"%@%@",[dictionary objectForKey:@"name"], [dictionary objectForKey:@"id"]];
                [cityDic setObject:[dictionary objectForKey:@"id"] forKey:[dictionary objectForKey:@"name"]];
            }
        }
        }
        NSLog(@"%@", cityDic);
    }
}

- (IBAction)county:(id)sender {
    [self showActionPickerWithTitle:@"Страна" andDataSourse:countryMutableArray];
    curAct=1;
}

- (IBAction)citykurort:(id)sender {
    if ([sityMutableArray count]==0&&countyId==nil) {
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Выбор города"message:@"Сначала выберите Страну" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }else if ([sityMutableArray count]==0){
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Выбор города"message:@"Данные для этой страны отсутствуют" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }else{
    [self showActionPickerWithTitle:@"Город" andDataSourse:sityMutableArray];
    curAct=2;
    }
}

- (IBAction)dateIn:(id)sender {
    [self atata];
    curAct=3;
}

- (IBAction)dateOut:(id)sender {
    [self atata];
    curAct=4;
}

- (IBAction)hotCat:(id)sender {
    [self showActionPickerWithTitle:@"Категория отеля" andDataSourse:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"Другая", nil]];
    curAct=5;
}

- (IBAction)find:(id)sender {
    if (budget_min==nil) {
        budget_min=@"0";
    }
    if (budget_max==nil) {
        budget_max=@"10000";
    }

    if (countyId==nil) {
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:@"Заказ тура"message:@"Заполните поле Страны" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [myAlert show];
    }else{
    
        NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithObject:countyId forKey:@"country_id"];
        if (cityId!=0) {
            [jsonDict setObject:cityId forKey:@"resort_id"];
        }
        cityId=nil;
    
//    [jsonDict setObject:dateInString forKey:@"date_check_in"];
//    [jsonDict setObject:dateOutString forKey:@"date_check_out"];
    [jsonDict setObject:budget_min forKey:@"budget_min"];
    [jsonDict setObject:budget_max forKey:@"budget_max"];
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
    sendURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=searchTours&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@", sendURL);
    [self performSegueWithIdentifier:@"result" sender:nil];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"result"])
    {
        SpecialOffersViewController *vc = [segue destinationViewController];
        vc.url=sendURL;
        NSLog(@"%@", sendURL);
    }
}

-(void)showActionPickerWithTitle:(NSString *)title andDataSourse:(NSArray *)dataSourseArray
{
    actionPicker = [[UNActionPicker alloc] initWithItems:dataSourseArray title:title];
    [actionPicker setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionPicker setCloseButtonTitle:@"Ok" color:[UIColor blackColor]];
    actionPicker.delegate = self;
    [actionPicker showInView:self.view];
}

- (void)didSelectItem:(id)item
{
    if (curAct==1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        countyId = countyDic[item];
        NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:countyDic[item] forKey:@"country_id"];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getCities&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        connection2 = [[NSURLConnection alloc] initWithRequest:request1 delegate:self];
        if (connection2)
        {
            NSLog(@"Connecting...");
            receivedData =[NSMutableData data];
        }
        else
        {
            NSLog(@"Connection error!");
        }
        [_countyBt setTitle:item forState:UIControlStateNormal];
    }else if (curAct==2){
        [_cityBt setTitle:item forState:UIControlStateNormal];
        cityId = cityDic[item];
    }else if (curAct==3){
        [_dateInBt setTitle:item forState:UIControlStateNormal];
    }else if (curAct==4){
        [_dateOutBt setTitle:item forState:UIControlStateNormal];
    }else if (curAct==5){
        [_hotelCategory setTitle:[NSString stringWithFormat:@"Категория отеля %@", item] forState:UIControlStateNormal];
        hotCategory=item;
         }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

-(void)atata
{
    
    pickerAction = [[UIActionSheet alloc] initWithTitle:@"Date" delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake ( 0.0, 44.0, 0.0, 0.0)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    //format datePicker mode. in this example time is used
    datePicker.datePickerMode = UIDatePickerModeDate;
    [dateFormatter setDateFormat:@"MM/dd/YYYY"];
    toolbarPicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbarPicker.barStyle=UIBarStyleBlackOpaque;
    [toolbarPicker sizeToFit];
    NSMutableArray *itemsBar = [[NSMutableArray alloc] init];
    //calls DoneClicked
    UIBarButtonItem *bbitem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneClicked)];
    [itemsBar addObject:bbitem];
    [toolbarPicker setItems:itemsBar animated:YES];
    [pickerAction addSubview:toolbarPicker];
    [pickerAction addSubview:datePicker];
    [pickerAction showInView:self.view];
    [pickerAction setBounds:CGRectMake(0,0,320, 464)];
}

-(IBAction)dateChanged{
    //format date
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] ];
    
    //set date format
    [FormatDate setDateFormat:@"YYYY-MM-dd"];
    //update date textfield
    if (curAct==3) {
        dateInString=[FormatDate stringFromDate:[datePicker date]];
        [_dateInBt setTitle:dateInString forState:UIControlStateNormal];
    }else if (curAct==4){
        dateOutString=[FormatDate stringFromDate:[datePicker date]];
        [_dateOutBt setTitle:dateOutString forState:UIControlStateNormal];
    }
    date = [FormatDate stringFromDate:[datePicker date]];
    NSLog(@"%@", date);
    
}

-(BOOL)closeDatePicker:(id)sender{
    [pickerAction dismissWithClickedButtonIndex:0 animated:YES];
    //[date resignFirstResponder];
    
    return YES;
}

//action when done button is clicked
-(IBAction)DoneClicked{
    [self closeDatePicker:self];
    datePicker.frame=CGRectMake(0, 44, 320, 416);
    [self dateChanged];
    
}

//returns number of components in pickerview
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

//returns number of rows in date picker
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 40;
}

- (void)viewDidUnload {
    [self setPriceFrom:nil];
    [self setPriceTo:nil];
    [self setCheckInDate:nil];
    [self setCheckOutDate:nil];
    [self setDateInBt:nil];
    [self setDateOutBt:nil];
    [self setCountyBt:nil];
    [self setCityBt:nil];
    [super viewDidUnload];
}
@end
