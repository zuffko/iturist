//
//  SearchVC.h
//  iTurist
//
//  Created by zufa on 9/9/13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"
#import "UNActionPicker.h"

@interface SearchVC : UIViewController <UITextFieldDelegate, UIPickerViewDelegate>{
    NSMutableData *receivedData;
}

@property (weak, nonatomic) IBOutlet UIButton *countyBt;
@property (weak, nonatomic) IBOutlet UIButton *cityBt;
@property (weak, nonatomic) IBOutlet UIButton *dateInBt;
@property (weak, nonatomic) IBOutlet UIButton *dateOutBt;
@property (weak, nonatomic) IBOutlet UIButton *hotelCategory;



@property (weak, nonatomic) IBOutlet UITextField    *priceFrom;
@property (weak, nonatomic) IBOutlet UITextField    *priceTo;
@property (weak, nonatomic) IBOutlet UIButton       *checkOutDate;
@property (weak, nonatomic) IBOutlet UIButton       *checkInDate;


@end
