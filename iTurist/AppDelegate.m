//
//  AppDelegate.m
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIImage *image = [UIImage imageNamed:@"action_bar.png"];
        [[UINavigationBar appearance] setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    UIImage *image2 = [UIImage imageNamed:@"tab_bar.png"];
    [[UITabBar appearance] setBackgroundImage:image2];
    return YES;
}

@end
