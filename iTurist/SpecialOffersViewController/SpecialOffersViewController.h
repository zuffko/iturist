//
//  SpecialOffersViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"
#import "MyCell.h"

#import "TourDescriptionScrollVC.h"

@interface SpecialOffersViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableData *receivedData;
    UIRefreshControl *objrefreshControl;
}
@property (nonatomic, retain) NSURL *url;
@property (strong, nonatomic)   IBOutlet UITableView        *specialOffersTableView;
@property (nonatomic) int page;
@property (nonatomic, retain) NSMutableData *infoData;
@property (nonatomic, retain) NSMutableString *stringParamReqest;

@property (nonatomic, retain) NSDictionary  *JSONresponse;
@property (nonatomic, retain) NSDictionary  *response;
@property (nonatomic, retain) NSDictionary  *tours;
@property (nonatomic, retain) NSDictionary  *dictionary;
@property (nonatomic, retain) NSMutableURLRequest *request;
@property (nonatomic, retain) NSMutableArray       *arrayTours;
@property (nonatomic, retain) NSMutableArray       *tempArrayTours;
@property (nonatomic, retain) NSArray       *hotTours;

@property (nonatomic, retain) NSArray       *tourIDArray;
@property (nonatomic, retain) NSArray       *tourPriceLabelArray;
@property (nonatomic, retain) NSArray       *nameOfTheTourLabelArray;
@property (nonatomic, retain) NSArray       *locationTourLabelArray;
@property (nonatomic, retain) NSArray       *dateOfDepartureLabelArray;
@property (nonatomic, retain) NSArray       *numberOfPeopleLabelArray;
@property (nonatomic, retain) NSArray       *lengthOfStayLabelArray;
@property (nonatomic, retain) NSArray       *hotRoundLabelArray;
@property (nonatomic, retain) NSArray       *presentationTourImageArray;
@property (nonatomic, retain) NSArray       *countryNameArray;
@property (nonatomic, retain) NSArray       *sityNameArray;
@property (nonatomic, retain) NSArray       *starsArray;

@property (nonatomic, retain) NSMutableArray       *tourPriceLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *nameOfTheTourLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *locationTourLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *dateOfDepartureLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *numberOfPeopleLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *lengthOfStayLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *hotRoundLabelMutableArray;
@property (nonatomic, retain) NSMutableArray       *presentationTourImageMutableArray;

@property (nonatomic, retain) NSMutableArray       *countryNameMutableArray;
@property (nonatomic, retain) NSMutableArray       *sityNameMutableArray;
@property (nonatomic, retain) NSMutableArray       *starsMutableArray;

@property (nonatomic, retain) NSMutableArray    *tourIDMutableArray;
@property (nonatomic, retain) NSArray       *arrrayElements;


@end
