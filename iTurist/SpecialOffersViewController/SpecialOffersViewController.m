//
//  SpecialOffersViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "SpecialOffersViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "NSMutableDictionary+DeleteNill.h"

@interface SpecialOffersViewController (){
    SDWebImageManager *manager;
    int page;
}
@end

@implementation SpecialOffersViewController
@synthesize page;
@synthesize request;
@synthesize JSONresponse;
@synthesize response;
@synthesize tours;
@synthesize dictionary;
@synthesize arrayTours;
@synthesize tempArrayTours;
@synthesize url=_url;

- (void)viewDidLoad
{
    [super viewDidLoad];
    page = 10;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papeer_bg.png"]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    manager = [SDWebImageManager new];
    [self sendRequest];
    objrefreshControl = [[UIRefreshControl alloc]init];
    objrefreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"Pull to refresh"];
    [objrefreshControl addTarget:self action:@selector(refreshmyTableView) forControlEvents:UIControlEventValueChanged];
    [_specialOffersTableView  addSubview:objrefreshControl];
    if (_url) {
        self.navigationItem.rightBarButtonItem=nil;
    }
    
}

-(void)refreshmyTableView
{
    [self sendRequest];
}

-(void)sendRequest
{
    NSLog(@"sendRequest %@", _url);
     
    if (_url==nil){
        NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"1",@"page_number", [NSString stringWithFormat:@"%d", page],@"items_on_page", @"1", @"only_hot",  nil];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:jsonDict];
        
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://iturist.kz/mobile.php?methodName=getTours&json=%@", jsonString]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            NSLog(@"Connecting...");
            receivedData =[NSMutableData data];
        }
        else
        {
            NSLog(@"Connection error!");
        }


    }else{
        request = [NSMutableURLRequest requestWithURL:_url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            NSLog(@"Connecting...");
            receivedData =[NSMutableData data];
        }
        else
        {
            NSLog(@"Connection error!");
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    
    NSLog(@"%@", errorString);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *dataString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    tempArrayTours = [[[[[SBJsonParser alloc] init] objectWithString:dataString] objectForKey:@"response"] objectForKey:@"tours"];

    if ([tempArrayTours isKindOfClass:[NSNull class]]  && _url) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Поиск" message: @"Ничего не найдено :(" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [objrefreshControl endRefreshing];
        [alert show];
    }else{
   int k=0;
   arrayTours = [NSMutableArray new];
   while (k<[tempArrayTours count]) {
       [arrayTours addObject: [[tempArrayTours objectAtIndex:k] dictionaryByReplacingNullsWithStrings]];
       k++;
   }
    [self createTableView];
    //NSLog(@"%@", arrayTours);
    }
}

#pragma mark - createTableView

- (void)createTableView
{
    self.title = @"Туры";
    _specialOffersTableView.dataSource = self;
    _specialOffersTableView.delegate = self;
    [self.view addSubview:_specialOffersTableView];
    [_specialOffersTableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [objrefreshControl endRefreshing];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayTours.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyCell";
    MyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[MyCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:CellIdentifier];
    }
    cell.tourPriceLabel.text = [NSString stringWithFormat:@"$ %@",[arrayTours objectAtIndex:indexPath.row][@"price_usd"]];
    cell.nameOfTheTourLabel.text = [arrayTours objectAtIndex:indexPath.row][@"hotel_name"];
    cell.locationTourLabel.font = [UIFont systemFontOfSize:14];
    cell.locationTourLabel.textColor = [UIColor grayColor];
    
    NSString *cityName =[NSString new];
    if ([arrayTours objectAtIndex:indexPath.row][@"city_name"] == [NSNull null])
    {
        cityName = @" ";
    }else{
        cityName = [arrayTours objectAtIndex:indexPath.row][@"city_name"];
    }
    cell.locationTourLabel.text     = [NSString stringWithFormat:@"%@ > %@",[arrayTours objectAtIndex:indexPath.row][@"country_name"], cityName];
    
    if ([[arrayTours objectAtIndex:indexPath.row][@"depature_date"] isKindOfClass:[NSString class]]) {
        cell.dateOfDepartureLabel.text  = [arrayTours objectAtIndex:indexPath.row][@"depature_date"];
    }
    cell.numberOfPeopleLabel.text   = [NSString stringWithFormat:@"%@ чел.", [arrayTours objectAtIndex:indexPath.row][@"persons_qty"]];
    cell.lengthOfStayLabel.text     = [NSString stringWithFormat:@"%@ ночей", [arrayTours objectAtIndex:indexPath.row][@"nights_qty"]];
    cell.hotRoundLabel.text         = @"Горящий Тур";
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [arrayTours objectAtIndex:indexPath.row][@"image_url"]]];
    [manager downloadWithURL:url
                     options:0
                    progress:^(NSUInteger receivedSize, long long expectedSize){}
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
     {
         if (image)
         {
             cell.presentationTourImage.image=image;
         }
     }];

    NSString *starsHotel = [arrayTours objectAtIndex:indexPath.row][@"stars_qty"];
    
    if ([starsHotel isKindOfClass:[NSString class]]) {
        int intValue = [starsHotel intValue];
        
        if (intValue == 0)
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"zero_stars@2x.png"];
        }
        else if (intValue == 1)
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"one_star@2x.png"];
        }
        else if (intValue == 2)
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"two_stars@2x.png"];
        }
        else if (intValue == 3)
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"three_stars@2x.png"];
        }
        else if (intValue == 4)
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"four_stars@2x.png"];
        }
        else
        {
            cell.hotelRatingsImage.image = [UIImage imageNamed:@"five_stars@2x.png"];
        }
    }
    
    if (indexPath.row == (page-1)) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //NSLog(@"%i", page);
        page+=10;
        [self sendRequest];
            
        //page+=10;
    }
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"MySegue" sender:nil];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MySegue"])
    {
        int index = [self.specialOffersTableView indexPathForSelectedRow].row;
        TourDescriptionScrollVC *detailViewController = [segue destinationViewController];
        _tourIDArray = [dictionary objectForKey:@"id"];
        detailViewController.moneyDescriptionLabelString=[NSString stringWithFormat:@"$ %@",[arrayTours objectAtIndex:index][@"price_usd"]];
        detailViewController.tourId = [arrayTours objectAtIndex:index][@"id"];
        detailViewController.nameTourDescriptionLabelString = [arrayTours objectAtIndex:index][@"hotel_name"];
        detailViewController.peopleNightDescriptionLabelString = [NSString stringWithFormat:@"%@  %@ ночей.", [NSString stringWithFormat:@"%@ чел.", [arrayTours objectAtIndex:index][@"persons_qty"]], [NSString stringWithFormat:@"%@ ночей", [arrayTours objectAtIndex:index][@"nights_qty"]]];
        detailViewController.imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [arrayTours objectAtIndex:index][@"image_url"]]];
        detailViewController.starsCount = [arrayTours objectAtIndex:index][@"stars_qty"];
    }
}

@end
