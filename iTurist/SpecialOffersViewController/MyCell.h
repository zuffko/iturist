//
//  MyCell.h
//  iTurist
//
//  Created by Sergey Gaponov on 22.04.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell


@property (strong, nonatomic)   IBOutlet UIImageView     *hotelRatingsImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *presentationTourImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *hotImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *separeteImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *calendarImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *nightImage;
@property (strong, nonatomic)   IBOutlet UIImageView     *peopleImage;

@property (strong, nonatomic)   IBOutlet UILabel     *tourPriceLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *nameOfTheTourLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *locationTourLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *dateOfDepartureLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *numberOfPeopleLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *lengthOfStayLabel;
@property (strong, nonatomic)   IBOutlet UILabel     *hotRoundLabel;


@end
