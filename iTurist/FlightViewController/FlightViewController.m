//
//  FlightViewController.m
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import "FlightViewController.h"

@interface FlightViewController ()  

@end

@implementation FlightViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Авиабилеты";
    self.navigationItem.titleView = label;
    self.navigationItem.titleView=label;
	// Do any additional setup after loading the view.
    [self web];
}

- (void)web
{
    NSURL *imaladecLink = [NSURL URLWithString:@"https://abacuswebstart.abacus.com.sg/silk-road-kazakhstan/flight-search.aspx"];
    [_abacusWebView loadRequest:[NSURLRequest requestWithURL:imaladecLink]];
    [self.view addSubview:_abacusWebView];
}


@end
