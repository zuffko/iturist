//
//  FlightViewController.h
//  iTurist
//
//  Created by Sergey Gaponov on 18.03.13.
//  Copyright (c) 2013 Sergey Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "SBJson.h"

@interface FlightViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIWebView *abacusWebView;

@end
